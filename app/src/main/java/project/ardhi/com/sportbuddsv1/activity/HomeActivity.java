package project.ardhi.com.sportbuddsv1.activity;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import butterknife.Bind;
import butterknife.ButterKnife;
import project.ardhi.com.sportbuddsv1.R;
import project.ardhi.com.sportbuddsv1.app.Config;
import project.ardhi.com.sportbuddsv1.fragment.EventFragment;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private ActionBarDrawerToggle toogle;
    private static final long DRAWER_CLOSE_DELAY_MS = 150;
    private static final String NAV_ITEM_ID = "navItemId";
    private int mNavItemId;
    private final android.os.Handler mDrawerActionHandler = new android.os.Handler();
    private EventFragment dashboardfragment;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");

                    Toast.makeText(getApplicationContext(), "GCM registration token: " + token, Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Config.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL

                    Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    Toast.makeText(getApplicationContext(), "Push notification is received!", Toast.LENGTH_LONG).show();
                }
            }
        };

        if (checkPlayServices()) {
            registerGCM();
        }

        init(savedInstanceState);

    }

    private void init(Bundle savedInstanceState){
//        setSupportActionBar(toolbar);

        //load saved navigation state if present
        if(null == savedInstanceState){
            mNavItemId = R.id.menu_dashboard;
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }

        navView.setItemIconTintList(null);
        navView.setNavigationItemSelectedListener(this);
        navView.getMenu().findItem(mNavItemId).setChecked(true);

        toogle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close);
        toogle.setDrawerIndicatorEnabled(true);
        toogle.syncState();
        drawerLayout.setDrawerListener(toogle);

        dashboardfragment = new EventFragment();
        if(savedInstanceState == null){
            getFragmentManager().beginTransaction().add(R.id.container_home,
                    dashboardfragment, EventFragment.TAG_EVENT).commit();
        }
    }

    private void navigate (final int itemId){
        Fragment fragment = null;
        String tag = null;
        switch (itemId){
            case R.id.menu_dashboard:
                fragment = dashboardfragment;
                tag = EventFragment.TAG_EVENT;
                break;
            case R.id.menu_profil:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_chat:
                Toast.makeText(this, "Chat", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_event:
                Toast.makeText(this, "Event", Toast.LENGTH_SHORT).show();
                fragment = new EventFragment();
                tag = EventFragment.TAG_EVENT;
                break;
            case R.id.menu_buddy:
                Toast.makeText(this, "Buddy", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_officialpage:
                Toast.makeText(this, "Official Page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_setting:
                Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_logout:
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                break;
        }
        if (fragment != null && tag != null) {
            changeFragment(fragment, tag);
        }
    }

    private void changeFragment(Fragment f, String s){
        getFragmentManager().beginTransaction().replace(R.id.container_home, f, s).addToBackStack(null).commit();
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        if(!item.isChecked()){
            item.setChecked(true);
            mNavItemId = item.getItemId();
            mDrawerActionHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigate(item.getItemId());
                }
            }, DRAWER_CLOSE_DELAY_MS);
        }

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toogle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(isDrawerOpened()){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            if(mNavItemId == R.id.menu_dashboard){
                super.onBackPressed();
            }else {
                backToHome();
            }
        }
    }

    private void backToHome(){
        mNavItemId = R.id.menu_dashboard;
        navView.getMenu().findItem(mNavItemId).setChecked(true);
        changeFragment(dashboardfragment, EventFragment.TAG_EVENT);
    }

    private boolean isDrawerOpened(){
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    // starting the service to register with GCM
    private void registerGCM() {
//        Intent intent = new Intent(this, GcmIntentService.class);
//        intent.putExtra("key", "register");
//        startService(intent);
    }

    private void handlenotif(Intent intent) {
//        AllData data = (AllData) intent.getSerializableExtra("message");

//        if (data != null) {
//            MyPreferenceManager.allDataArrayList.add(0, data);
            Toast.makeText(getApplicationContext(), "test", Toast.LENGTH_LONG).show();
//            LogAllFragment.mAdapter.notifyDataSetChanged();
//            if (LogAllFragment.mAdapter.getItemCount() > 1) {
//                LogAllFragment.recyclerView.getLayoutManager().smoothScrollToPosition(LogAllFragment.recyclerView, null, LogAllFragment.mAdapter.getItemCount() - 1);
//            }
//        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
//        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
//        AppEventsLogger.deactivateApp(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(toogle.onOptionsItemSelected(menuItem))
            return true;
        switch (menuItem.getItemId()) {
            case R.id.action_logout:
//                MyApplication.getInstance().logout();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

}