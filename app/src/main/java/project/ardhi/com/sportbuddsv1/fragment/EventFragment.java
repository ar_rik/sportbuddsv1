package project.ardhi.com.sportbuddsv1.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import project.ardhi.com.sportbuddsv1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG_EVENT = "event_fragment";

    public EventFragment() {
        // Required empty public constructor
    }

//    LatLng tempat;
//    String nama = "Tempat Olahraga";
//    GoogleMap googleMap;
//    private FragmentActivity myContext;
//    private GoogleMap mMap;
    String lang = "-6.121435";
    String lat = "106.8227";
    private ImageView iv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        iv = (ImageView) view.findViewById(R.id.map);
        Picasso.with(getActivity()).load("http://maps.google.com/maps/api/staticmap?center="+lang+","+lat+"&zoom=9&size=300x200&sensor=false&markers=color:red%7Clabel:C%7C-6.121435,106.8227").fit().into(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "dfdsfsdf", Toast.LENGTH_SHORT).show();
//                new MapActivity(lat, lang);
//                startActivity(new Intent(getActivity(), MapActivity.class));
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {

    }

//    @Override
//    public void onAttach(Context context) {
//        myContext=(FragmentActivity) context;
//        SupportMapFragment fm = (SupportMapFragment) myContext.getSupportFragmentManager().findFragmentById(R.id.map);
//        googleMap = fm.getMap();
//        super.onAttach(context);
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//    }
}
