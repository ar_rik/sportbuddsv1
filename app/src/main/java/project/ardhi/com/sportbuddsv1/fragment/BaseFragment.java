package project.ardhi.com.sportbuddsv1.fragment;

import android.app.Fragment;

import butterknife.ButterKnife;

/**
 * Created by erdearik on 2/2/16.
 */
public class BaseFragment extends Fragment {


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
