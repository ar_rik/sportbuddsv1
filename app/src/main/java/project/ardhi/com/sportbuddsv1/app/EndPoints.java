package project.ardhi.com.sportbuddsv1.app;

/**
 * Created by erdearik on 5/7/16.
 */
public class EndPoints {
    public static final String BASE_URL = "https://server-sportbudds.c9users.io/v1";
    public static final String LOGIN = BASE_URL + "/login";
    public static final String TASKS = BASE_URL + "/tasks";
    public static final String USER = BASE_URL + "/users";
    public static final String REGISTER = BASE_URL + "/register";
    public static final String CHAT_ROOMS = BASE_URL + "/chat/room/user";
    public static final String CHAT_ROOM_MESSAGE = BASE_URL + "/chat";
    public static final String CHAT_THREAD = BASE_URL + "/chat/_ID_";
}
